
#include <ros/ros.h>
#include <std_msgs/String.h>
#include <string>
#include <robotics_ur/ur_move.h>

ros::Publisher joint_pub;

bool cyton_move_all_joints(robotics_ur::ur_move::Request &req, robotics_ur::ur_move::Response &res) {
        //rostopic pub /ur_driver/URScript std_msgs/String "data: 'movej([0,-2.4260,-1.2741,-0.6632,1.5708,0],a = 0.75,v=0.25,r=0.001)'"
    std_msgs::String stringMsg;
    std::stringstream ss;
    ss << "movej([";// << req.q[0]
    if(req.q.size() != 6){
        return false;
    }
    ss << req.q[0] << ",";
    ss << req.q[1] << ",";
    ss << req.q[2] << ",";
    ss << req.q[3] << ",";
    ss << req.q[4] << ",";
    ss << req.q[5];

    ss << "], a = 0.75, v=0.20, r=0.001)";

    stringMsg.data = ss.str();

    joint_pub.publish(stringMsg);

    std::cout << "Moving Joints To: " << std::endl;
   for(int i=0; i < 6; ++i){
	std::cout << "Joint[" << i << "] = " << req.q[i] << " Rads" << std::endl;
   }
   //std::cout << "With Acceleration: " << req.acc << " and Velocity: " << req.vel << std::endl;
   res.success = true;
    return true;
}


int main(int argc, char** argv)
{
    ros::init(argc, argv, "robotics_ur");
    ros::NodeHandle node_handle("~");
    ros::NodeHandle nh;
    joint_pub = nh.advertise<std_msgs::String>("/ur_driver/URScript",1);



    ros::ServiceServer service = nh.advertiseService("ur_move",cyton_move_all_joints);
    ros::AsyncSpinner spinner(0);
    spinner.start();
    ros::waitForShutdown();


    return 0;
}
